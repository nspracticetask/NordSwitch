# Use the official NGINX image as the base image
FROM nginx:latest

# Replace default NGINX configuration with a custom configuration that responds with HTTP 200 for all requests
RUN echo "server { listen 80 default_server; return 200 'Hello, World!'; }" > /etc/nginx/conf.d/default.conf
